﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Mythostock.Controls
{
    /// <summary>
    /// Interaction logic for InputWindow.xaml
    /// </summary>
    public partial class InputWindow : Window
    {
        public InputWindow()
        {
            InitializeComponent();
        }

        public string Message { get; set; }

        public string Input { get; set; }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);

            this.MessageTextBlock.Text = this.Message;
            this.InputTextBox.Text = this.Input;
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            this.Message = this.MessageTextBlock.Text;
            this.Input = this.InputTextBox.Text;
        }

        public static string ShowDialog(string title, string message, string defaultInput)
        {
            var window = new InputWindow()
            {
                Title = title,
                Message = message,
                Input = defaultInput,
            };
            window.ShowDialog();
            return window.Input;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}