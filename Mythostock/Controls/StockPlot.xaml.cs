﻿using GalaSoft.MvvmLight.Threading;
using Mythostock.Stockfighter;
using OxyPlot;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Mythostock.Controls
{
    /// <summary>
    /// Interaction logic for StockPlot.xaml
    /// </summary>
    public partial class StockPlot : UserControl
    {
        private readonly PlotModel _model;
        private readonly LineSeries _lastPriceLineSeries;
        private readonly HighLowSeries _lastPriceHighLowSeries;
        private DateTime _startTime;

        public StockPlot()
        {
            InitializeComponent();

            AccountManager.Instance.QuoteReceived += Instance_QuoteReceived;
            AccountManager.Instance.QuotesBlockReceived += Instance_QuotesBlockReceived;

            _model = new PlotModel();
            _lastPriceLineSeries = new LineSeries();
            _model.Series.Add(_lastPriceLineSeries);

            _lastPriceHighLowSeries = new HighLowSeries()
            {
                StrokeThickness = 2,
                TickLength = 10,
            };
            _model.Series.Add(_lastPriceHighLowSeries);

            this.PlotView.Model = _model;
        }

        public static readonly DependencyProperty StockSymbolProperty = DependencyProperty.Register("StockSymbol", typeof(string), typeof(StockPlot), new PropertyMetadata(null, OnStockSymbolChangedStatic));

        public string StockSymbol
        {
            get { return (string)GetValue(StockSymbolProperty); }
            set { SetValue(StockSymbolProperty, value); }
        }

        private static void OnStockSymbolChangedStatic(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var that = (StockPlot)d;
            that.OnStockSymbolChanged(d, e);
        }

        private void OnStockSymbolChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            _startTime = DateTime.UtcNow;
            _model.Title = this.StockSymbol;
            _lastPriceLineSeries.Points.Clear();
            _lastPriceHighLowSeries.Items.Clear();
            this.PlotView.InvalidatePlot();
        }

        private void Instance_QuoteReceived(object sender, global::Stockfighter.Stock.Quote e)
        {
        }

        private void Instance_QuotesBlockReceived(object sender, EventArgs e)
        {
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
                var evaluation = AccountManager.Instance.GetEvaluation(this.StockSymbol);
                if (evaluation == null)
                    return;

                _lastPriceHighLowSeries.Items.Clear();
                _lastPriceHighLowSeries.Items.AddRange(
                    evaluation.Quotes.Select(x => new HighLowItem((x.Timestamp - _startTime).TotalSeconds, x.Max, x.Min)));

                _lastPriceLineSeries.Points.Clear();
                _lastPriceLineSeries.Points.AddRange(
                    evaluation.Quotes.Select(x => new DataPoint((x.Timestamp - _startTime).TotalSeconds, x.Mean)));

                this.PlotView.InvalidatePlot();
            });
        }
    }
}