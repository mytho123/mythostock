﻿using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;
using Mythostock.Stockfighter;
using Mythostock.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Mythostock
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            DispatcherHelper.Initialize();
            Task.Run(async () => await AccountManager.Instance.Init());

            var timer = new Timer(OnTimerTick, null, TimeSpan.FromSeconds(10), TimeSpan.FromSeconds(10));
        }

        private void OnTimerTick(object state)
        {
            NativeMethods.SetThreadExecutionState(NativeMethods.EXECUTION_STATE.ES_DISPLAY_REQUIRED);
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);

            Mythostock.Properties.Settings.Default.Save();
        }
    }
}