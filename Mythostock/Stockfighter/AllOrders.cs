﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Threading;
using Mythostock.Controls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Mythostock.Stockfighter
{
    internal class AllOrders
    {
        public bool OK { get; set; }

        public string Venue { get; set; }

        public ICollection<JObject> Orders { get; set; }
    }
}