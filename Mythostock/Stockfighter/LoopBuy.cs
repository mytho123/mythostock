﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Threading;
using Mythostock.Controls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Mythostock.Stockfighter
{
    internal class LoopBuy : ViewModelBase
    {
        private LoopBuy()
        {
            this.BuyCommand = new RelayCommand(BuyCommand_Execute);
        }

        public static LoopBuy Instance { get; } = new LoopBuy();

        private int _totalToBuy = 100000;

        public int TotalToBuy
        {
            get { return _totalToBuy; }
            set { this.Set(ref _totalToBuy, value); }
        }

        private int _blockAmount = 100;

        public int BlockAmount
        {
            get { return _blockAmount; }
            set { this.Set(ref _blockAmount, value); }
        }

        private double _secondsToWait = 1;

        public double SecondsToWait
        {
            get { return _secondsToWait; }
            set { this.Set(ref _secondsToWait, value); }
        }

        private double _price = 0;

        public double Price
        {
            get { return _price; }
            set { this.Set(ref _price, value); }
        }

        private string _stockToBuy;

        public string StockToBuy
        {
            get { return _stockToBuy; }
            set { this.Set(ref _stockToBuy, value); }
        }

        public RelayCommand BuyCommand { get; }

        private void BuyCommand_Execute()
        {
            Task.Run(async () => await BuyCommand_ExecuteAsync());
        }

        private async Task BuyCommand_ExecuteAsync()
        {
            var account = AccountManager.Instance.GetAccount();
            var random = new Random();

            while (AccountManager.Instance.GetBalanceForStock(this.StockToBuy)?.Number < this.TotalToBuy)
            {
                var amount = random.Next(this.BlockAmount);
                await AccountManager.Instance.WaitLimitOrder(this.StockToBuy, this.Price, amount);
                await Task.Delay((int)(_secondsToWait * 1000));

                //var order = await account.Buy(this.StockToBuy, (int)(this.Price * 100), this.BlockAmount);
                //await Task.Delay((int)(_secondsToWait * 1000));
                //await order.Cancel();

                //order = await account.GetOrder(order.Id, this.StockToBuy);
                //this.TotalBought += order.TotalFilled;
            }
        }
    }
}