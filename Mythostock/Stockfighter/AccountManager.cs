﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Threading;
using Mythostock.Controls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Stockfighter;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Mythostock.Stockfighter
{
    internal class AccountManager : ViewModelBase
    {
        private TickerTape _tape;
        private OrderFeed _feed;

        private AccountManager()
        {
        }

        #region props

        public static AccountManager Instance { get; } = new AccountManager();

        public string ApiKey
        {
            get { return Properties.Settings.Default.ApiKey; }
            set
            {
                Properties.Settings.Default.ApiKey = value;
                this.OnSettingChanged();
            }
        }

        public string TradingAccount
        {
            get { return Properties.Settings.Default.TradingAccount; }
            set
            {
                Properties.Settings.Default.TradingAccount = value;
                this.OnSettingChanged();
            }
        }

        public string Venue
        {
            get { return Properties.Settings.Default.Venue; }
            set
            {
                Properties.Settings.Default.Venue = value;
                this.OnSettingChanged();
            }
        }

        private AccountStatus _status = AccountStatus.ApiIsDown;

        public AccountStatus Status
        {
            get { return _status; }
            set { this.Set(ref _status, value); }
        }

        public string ProxyAddress
        {
            get { return Properties.Settings.Default.ProxyAddress; }
            set
            {
                Properties.Settings.Default.ProxyAddress = value;
                this.OnSettingChanged();
            }
        }

        public string ProxyUser
        {
            get { return Properties.Settings.Default.ProxyUser; }
            set
            {
                Properties.Settings.Default.ProxyUser = value;
                this.OnSettingChanged();
            }
        }

        public string ProxyPassword
        {
            get { return Properties.Settings.Default.ProxyPassword; }
            set
            {
                Properties.Settings.Default.ProxyPassword = value;
                this.OnSettingChanged();
            }
        }

        public ObservableCollection<string> Levels { get; } = new ObservableCollection<string>()
        {
            "first_steps", "chock_a_block",
        };

        public string Level
        {
            get { return Properties.Settings.Default.Level; }
            set
            {
                Properties.Settings.Default.Level = value;
                this.OnSettingChanged();
            }
        }

        #endregion props

        public ObservableCollection<Stock> Stocks { get; } = new ObservableCollection<Stock>();

        private void OnSettingChanged([CallerMemberName]string propertyName = null)
        {
            Properties.Settings.Default.Save();
            this.RaisePropertyChanged(propertyName);

            Task.Run(async () =>
            {
                switch (propertyName)
                {
                    case nameof(this.ProxyAddress):
                    case nameof(this.ProxyUser):
                    case nameof(this.ProxyPassword):
                        this.InitProxy();
                        break;

                    case nameof(this.ApiKey):
                    case nameof(this.Level):
                        await this.InitLevel();
                        break;
                }
                await this.UpdateStatus();
            });
        }

        public async Task Init()
        {
            this.InitProxy();
            await this.InitLevel();
            await this.UpdateStatus();
        }

        public void InitProxy()
        {
            if (string.IsNullOrEmpty(this.ProxyAddress))
                WebRequest.DefaultWebProxy = null;
            else
                WebRequest.DefaultWebProxy = new WebProxy(this.ProxyAddress, true, null, new NetworkCredential(this.ProxyUser, this.ProxyPassword));
        }

        private async Task InitLevel()
        {
            if (!string.IsNullOrEmpty(this.ApiKey) && !string.IsNullOrEmpty(this.Level))
            {
                var gm = new GameMaster(this.ApiKey);
                var level = await gm.GetLevel(this.Level);
                if (level != null)
                {
                    this.TradingAccount = level.Account;
                    this.Venue = level.Venues.FirstOrDefault();
                }
            }
        }

        private async Task UpdateStatus()
        {
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
                this.Stocks.Clear();
            });

            var heartbeat = await this.ProcessRequestAsync<HeartbeatResponse>("https://api.stockfighter.io/ob/api/heartbeat");
            if (!heartbeat.OK)
            {
                this.Status = AccountStatus.ApiIsDown;
                return;
            }

            var venueUp = await this.ProcessRequestAsync<VenueUp>($"https://api.stockfighter.io/ob/api/venues/{this.Venue}/heartbeat");
            if (!venueUp.OK)
            {
                this.Status = AccountStatus.VenueIsDown;
                return;
            }

            var orders = await this.ProcessRequestAsync<AllOrders>($"https://api.stockfighter.io/ob/api/venues/{this.Venue}/accounts/{this.TradingAccount}/orders");
            if (!orders.OK)
            {
                this.Status = AccountStatus.WrongCredentials;
                return;
            }

            this.Status = AccountStatus.OK;
            this.RestartQuotesFeed();
            this.RestartOrdersFeed();

            var venue = new Venue(this.Venue);
            var stocks = await venue.GetStocks();
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
                this.Stocks.Clear();
                foreach (var stock in stocks)
                    this.Stocks.Add(stock);
            });
        }

        private async Task<TResult> ProcessRequestAsync<TResult>(string requestUrl)
        {
            var response = await this.ProcessRequestAsync(requestUrl);
            return JsonConvert.DeserializeObject<TResult>(response);
        }

        private async Task<string> ProcessRequestAsync(string requestUrl)
        {
            Debug.WriteLine($"GET {requestUrl}");

            var request = WebRequest.CreateHttp(requestUrl);
            request.Headers.Add("X-Starfighter-Authorization", this.ApiKey);

            HttpWebResponse response;
            try
            {
                response = await request.GetResponseAsync() as HttpWebResponse;
            }
            catch (WebException e)
            {
                response = e.Response as HttpWebResponse;
            }

            string responseString;
            using (var stream = response.GetResponseStream())
            {
                using (var reader = new StreamReader(stream))
                {
                    responseString = reader.ReadToEnd();
                }
            }
            return responseString;
        }

        public Account GetAccount()
        {
            return new Account(this.Venue, this.TradingAccount, this.ApiKey);
        }

        #region Quotes

        public event EventHandler<Stock.Quote> QuoteReceived;

        public event EventHandler<EventArgs> QuotesBlockReceived;

        private readonly ConcurrentDictionary<string, StockEvalutation> _stockEvaluations = new ConcurrentDictionary<string, StockEvalutation>();

        private readonly TimeSpan _quotesEvaluationPeriod = TimeSpan.FromMilliseconds(500);
        private Timer _quotesEvaluationTimer;

        private Stock.Quote _lastQuote;

        public Stock.Quote LastQuote
        {
            get { return _lastQuote; }
            set { this.Set(ref _lastQuote, value); }
        }

        public ObservableCollection<StockEvalutation> Evaluations { get; } = new ObservableCollection<StockEvalutation>();

        public StockEvalutation GetEvaluation(string stockSymbol)
        {
            StockEvalutation evaluation;
            _stockEvaluations.TryGetValue(stockSymbol, out evaluation);
            return evaluation;
        }

        private void RestartQuotesFeed()
        {
            _tape?.Stop();
            _tape = new TickerTape(this.TradingAccount, this.Venue);
            _tape.QuoteReceived += OnQuoteReceived;
            _tape.ErrorOccured += OnQuoteError;
            _tape.Start();

            _quotesEvaluationTimer?.Dispose();
            _quotesEvaluationTimer = new Timer(OnQuotesBlockTimer, null, TimeSpan.Zero, _quotesEvaluationPeriod);
        }

        private void OnQuoteReceived(object sender, Stock.Quote quote)
        {
            this.LastQuote = quote;
            if (this.QuoteReceived != null)
                this.QuoteReceived(this, quote);

            var evaluation = _stockEvaluations.GetOrAdd(quote.Symbol, x =>
            {
                var eval = new StockEvalutation(x);
                DispatcherHelper.CheckBeginInvokeOnUI(() => this.Evaluations.Add(eval));
                return eval;
            });
            evaluation.OnQuoteReceived(quote);
        }

        private void OnQuotesBlockTimer(object state)
        {
            foreach (var evaluation in _stockEvaluations.Values)
                evaluation.Reevaluate();

            if (this.QuotesBlockReceived != null)
                this.QuotesBlockReceived(this, new EventArgs());
        }

        private void OnQuoteError(object sender, string e)
        {
            var tape = sender as TickerTape;
            tape.Start();
        }

        #endregion Quotes

        #region Current Balance

        private readonly ConcurrentDictionary<string, StockBalance> _currentBalance = new ConcurrentDictionary<string, StockBalance>();

        public ObservableCollection<StockBalance> CurrentBalance { get; } = new ObservableCollection<StockBalance>();

        public StockBalance GetBalanceForStock(string stockSymbol)
        {
            return _currentBalance.GetOrAdd(
                stockSymbol,
                x =>
                {
                    var createdBalance = new StockBalance(x);
                    DispatcherHelper.CheckBeginInvokeOnUI(() => this.CurrentBalance.Add(createdBalance));
                    return createdBalance;
                });
        }

        private void OnOrderReceived(object sender, OrderMessage message)
        {
            var order = message.Order;
            if (order.Account != this.TradingAccount)
                return;

            var balance = this.GetBalanceForStock(order.Symbol);

            foreach (var fill in order.Fills)
                balance.RegisterFill(fill);

            if (!order.IsOpen)
            {
                this.ReleaseSemaphore(this.GetSemaphoreForOrder(order.Id));
            }
        }

        private void RestartOrdersFeed()
        {
            _feed?.Stop();
            _feed = new OrderFeed(this.TradingAccount, this.Venue);
            _feed.OrderReceived += OnOrderReceived;
            _feed.ErrorOccured += OnOrderError;
            _feed.Start();
        }

        private void OnOrderError(object sender, string e)
        {
            this.RestartOrdersFeed();
            this.ReleaseAllSemaphores();
        }

        #endregion Current Balance

        #region Orders

        public async Task WaitLimitOrder(string stock, double price, int quantity)
        {
            var order = await this.GetAccount().Buy(stock, Convert.ToInt32(Math.Round(price * 100)), quantity, "limit");
            await this.GetSemaphoreForOrder(order.Id).WaitAsync();
        }

        private readonly ConcurrentDictionary<int, SemaphoreSlim> _ordersSemaphores = new ConcurrentDictionary<int, SemaphoreSlim>();

        private SemaphoreSlim GetSemaphoreForOrder(int orderId)
        {
            return _ordersSemaphores.GetOrAdd(
                orderId,
                x => new SemaphoreSlim(0, 1));
        }

        private void ReleaseSemaphore(SemaphoreSlim semaphore)
        {
            try { semaphore.Release(); }
            catch (SemaphoreFullException) { }
        }

        private void ReleaseAllSemaphores()
        {
            var semaphores = _ordersSemaphores.Values.ToArray();
            _ordersSemaphores.Clear();

            foreach (var semaphore in semaphores)
                this.ReleaseSemaphore(semaphore);
        }

        #endregion Orders
    }
}