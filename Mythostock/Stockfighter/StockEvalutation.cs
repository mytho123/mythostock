﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Threading;
using Mythostock.Controls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Stockfighter;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Mythostock.Stockfighter
{
    internal class StockEvalutation : ViewModelBase
    {
        private class QuoteComparer : IComparer<Stock.Quote>
        {
            public int Compare(Stock.Quote x, Stock.Quote y)
            {
                return DateTime.Compare(x.LastTradeTimestamp, y.LastTradeTimestamp);
            }
        }

        public class QuotesBlock
        {
            public QuotesBlock(string symbol, ICollection<Stock.Quote> quotes)
            {
                this.Symbol = symbol;
                this.Min = quotes.Min(x => x.LastTradePriceInCents);
                this.Mean = Convert.ToDouble(quotes.Sum(x => x.LastTradePriceInCents)) / quotes.Count;
                this.Max = quotes.Max(x => x.LastTradePriceInCents);
                this.Timestamp = quotes.Min(x => x.LastTradeTimestamp);
            }

            public int Max { get; }
            public double Mean { get; }
            public int Min { get; }
            public string Symbol { get; }
            public DateTime Timestamp { get; }
        }

        private readonly SortedSet<Stock.Quote> _currentQuotes = new SortedSet<Stock.Quote>(new QuoteComparer());
        private readonly TimeSpan _quotesSampling = TimeSpan.FromMilliseconds(500);

        public StockEvalutation(string stockSymbol)
        {
            this.StockSymbol = stockSymbol;
        }

        public string StockSymbol { get; }

        private QuotesBlock[] _quotes = new QuotesBlock[0];

        public QuotesBlock[] Quotes
        {
            get { return _quotes; }
            set { this.Set(ref _quotes, value); }
        }

        internal void OnQuoteReceived(Stock.Quote quote)
        {
            lock (_currentQuotes)
            {
                _currentQuotes.Add(quote);
            }
        }

        internal void Reevaluate()
        {
            var blocks = new List<QuotesBlock>();

            lock (_currentQuotes)
            {
                var lastQuoteDate = DateTime.MinValue;
                var quotesInBlock = new List<Stock.Quote>();
                foreach (var quote in _currentQuotes)
                {
                    if (quote.LastTradeTimestamp - lastQuoteDate > _quotesSampling)
                    {
                        lastQuoteDate = quote.LastTradeTimestamp;
                        if (quotesInBlock.Count != 0)
                        {
                            var block = new QuotesBlock(quotesInBlock.First().Symbol, quotesInBlock);
                            blocks.Add(block);
                            quotesInBlock = new List<Stock.Quote>();
                        }
                    }
                    else
                    {
                    }
                    quotesInBlock.Add(quote);
                }
            }

            this.Quotes = blocks.ToArray();
        }
    }
}