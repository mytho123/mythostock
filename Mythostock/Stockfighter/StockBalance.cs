﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Threading;
using Mythostock.Controls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Stockfighter;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Mythostock.Stockfighter
{
    internal class StockBalance : ViewModelBase
    {
        private readonly object _fillLock = new object();

        public StockBalance(string symbol)
        {
            this.Symbol = symbol;
        }

        public string Symbol { get; }

        private int _number = 0;

        public int Number
        {
            get { return _number; }
            private set { this.Set(ref _number, value); }
        }

        private double _averagePrice = 0;

        public double AveragePrice
        {
            get { return _averagePrice; }
            private set { this.Set(ref _averagePrice, value); }
        }

        public void RegisterFill(Order.Fill fill)
        {
            lock (_fillLock)
            {
                var totalPrice = this.Number * this.AveragePrice + fill.Quantity * Convert.ToDouble(fill.PriceInCents) / 100.0;
                this.Number += fill.Quantity;
                this.AveragePrice = totalPrice / this.Number;
            }
        }
    }
}